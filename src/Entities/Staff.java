package Entities;

public class Staff {
	
	private String _userID;
	private String _firstName;
	private String _lastName;
	private String _password;
	private Boolean _director;
	
	private Staff _supervisor;

	public String getUserID() {
		return _userID;
	}

	public void setUserID(String _userID) {
		this._userID = _userID;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String _firstName) {
		this._firstName = _firstName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String _lastName) {
		this._lastName = _lastName;
	}

	public String getPassword() {
		return _password;
	}

	public void setPassword(String _password) {
		this._password = _password;
	}

	public Staff getSupervisor() {
		return _supervisor;
	}

	public void setSupervisor(Staff _supervisor) {
		this._supervisor = _supervisor;
	}
	
	public void isDirector(Boolean director){
		this._director = director;
	}
	
	public Boolean isDirector(){
		return this._director;
	}
	
	public Boolean isValid(){
		if(this._supervisor==null && !this._director){
			return false;
		}
		return true;
	}
	
	public Boolean isAncestor(Staff staff){
		Staff checkStaff = _supervisor;
		
		while(checkStaff!=null){
			if(checkStaff.getUserID().equals(staff.getUserID())){
				return true;
			}else{
				checkStaff = checkStaff.getSupervisor();
			}
		}
		
		return false;
	}
}
