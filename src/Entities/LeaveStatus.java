package Entities;

public enum LeaveStatus {
	Pending, Rejected, Approved
}
