package Entities;
import java.util.Date;


public class Leave {
	private Staff _applyingStaff;
	private Date _fromDate;
	private Date _toDate;
	private LeaveStatus _status;
	private Staff _nextSupervisor;
	
	public Staff getApplyingStaff() {
		return _applyingStaff;
	}
	public void setApplyingStaff(Staff _applyingStaff) {
		this._applyingStaff = _applyingStaff;
	}
	public Date getFromDate() {
		return _fromDate;
	}
	public void setFromDate(Date _fromDate) {
		this._fromDate = _fromDate;
	}
	public Date getToDate() {
		return _toDate;
	}
	public void setToDate(Date _toDate) {
		this._toDate = _toDate;
	}
	public LeaveStatus getStatus() {
		return _status;
	}
	public void setStatus(LeaveStatus _status) {
		this._status = _status;
	}
	public Staff getNextSupervisor() {
		return _nextSupervisor;
	}
	public void setNextSupervisor(Staff _nextSupervisor) {
		this._nextSupervisor = _nextSupervisor;
	}
}
