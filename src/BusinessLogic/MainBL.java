package BusinessLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Resources.SingletonResource;
import Views.AndorseWindow;
import Views.AssignWindow;
import Views.LoginWindow;
import Views.MainWindow;
import Views.RequestWindow;
import Views.ResultWindow;
import Views.StaffWindow;

public class MainBL implements ActionListener {

	private LoginWindow loginWindow;
	private MainWindow mainWindow;
	private StaffWindow staffWindow;
	private AssignWindow assignWindow;
	private RequestWindow requestWindow;
	private AndorseWindow andorseWindow;
	private ResultWindow resultWindow;
	
	private void closeWindows(){
		if(staffWindow!=null){
			staffWindow.setVisible(false);
		}
		if(assignWindow!=null){
			assignWindow.setVisible(false);
		}
		if(requestWindow!=null){
			requestWindow.setVisible(false);
		}
		if(andorseWindow!=null){
			andorseWindow.setVisible(false);
		}
		if(resultWindow!=null){
			resultWindow.setVisible(false);
		}
	}

	public MainBL() {
		mainWindow = new MainWindow();
		loginWindow = new LoginWindow("Login");
	}

	public void programStart() {
		SingletonResource.initialize();

		showMainWindow();

		LoginBL loginBL = new LoginBL(mainWindow, loginWindow);

		loginBL.showLoginWindow();
	}

	public void showMainWindow() {
		mainWindow.addActionListener(this);
		mainWindow.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		switch (action) {
		case "staff":
			staffWindow = new StaffWindow("Staff");
			StaffBL staffBL = new StaffBL(mainWindow, staffWindow);
			staffBL.showStaffWindow();
			break;
		case "supervisor":
			assignWindow = new AssignWindow("Assign supervisor");
			AssignBL assignBL = new AssignBL(mainWindow, assignWindow);
			assignBL.showAssignWindow();
			break;
		case "apply":
			requestWindow = new RequestWindow("Apply leave");
			RequestBL requestBL = new RequestBL(mainWindow, requestWindow);
			requestBL.showRequestWindow();
			break;
		case "endorse":
			andorseWindow = new AndorseWindow("Endorse/Reject leave");
			AndorseBL andorseBL = new AndorseBL(mainWindow, andorseWindow);
			andorseBL.showAndorseWindow();
			break;
		case "result":
			resultWindow = new ResultWindow("My leaves");
			ResultBL resultBL = new ResultBL(mainWindow, resultWindow);
			resultBL.showResultWindow();
			break;
		case "logout":
			closeWindows();
			loginWindow = new LoginWindow("Login");
			LoginBL loginBL = new LoginBL(mainWindow, loginWindow);
			loginBL.showLoginWindow();
			break;
		default:
			break;
		}

	}
}
