package BusinessLogic;

import Views.MainWindow;
import Views.ResultWindow;

public class ResultBL {
	private MainWindow mainWindow;
	private ResultWindow resultWindow;
	
	public ResultBL(MainWindow mainWindow, ResultWindow resultWindow){
		this.mainWindow = mainWindow;
		this.resultWindow = resultWindow;
	}

	public void showResultWindow(){
		resultWindow.setClosable(true);
		mainWindow.getInternalPane().add(resultWindow);
		
		resultWindow.setVisible(true);
		
		resultWindow.setLeaves();
	}
}
