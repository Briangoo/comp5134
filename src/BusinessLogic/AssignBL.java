package BusinessLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Entities.Staff;
import Views.AssignWindow;
import Views.MainWindow;

public class AssignBL implements ActionListener {
	private MainWindow mainWindow;
	private AssignWindow assignWindow;
	
	public AssignBL(MainWindow mainWindow, AssignWindow assignWindow){
		this.mainWindow = mainWindow;
		this.assignWindow = assignWindow;
	}

	public void showAssignWindow(){
		this.assignWindow.addActionListener(this);
		
		assignWindow.setClosable(true);
		mainWindow.getInternalPane().add(assignWindow);
		
		assignWindow.setVisible(true);
		
		assignWindow.setStaffs();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		switch (action) {
		case "save":
			saveAssign();
			break;
		default:
			break;
		}
	}
	
	public void saveAssign(){
		Staff staff = assignWindow.getStaff();
		Staff supervisor = assignWindow.getSupervisor();
		
		if(staff==null){
			JOptionPane.showMessageDialog(assignWindow,
					"Staff is not selected.", "No staff",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		if(supervisor==null && !staff.isDirector()){
			JOptionPane.showMessageDialog(assignWindow,
					"Supervisor is not selected.", "No supervisor",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		staff.setSupervisor(supervisor);
		
		JOptionPane.showMessageDialog(assignWindow,
				"Supervisor assigned.", "Assign supervisor",
				JOptionPane.INFORMATION_MESSAGE);
	}
}
