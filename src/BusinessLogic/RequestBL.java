package BusinessLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JOptionPane;

import Entities.Leave;
import Entities.LeaveStatus;
import Resources.SingletonResource;
import Views.MainWindow;
import Views.RequestWindow;

public class RequestBL implements ActionListener {
	private RequestWindow requestWindow;
	private MainWindow mainWindow;

	public RequestBL(MainWindow mainWindow, RequestWindow requestWindow) {
		this.requestWindow = requestWindow;
		this.mainWindow = mainWindow;
	}

	public void showRequestWindow() {
		this.requestWindow.addActionListener(this);
		
		requestWindow.setClosable(true);
		mainWindow.getInternalPane().add(requestWindow);
		
		requestWindow.setVisible(true);
	}

	private void applyLeave() {
		Leave leave = new Leave();
		leave.setApplyingStaff(SingletonResource.getLoginStaff());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			leave.setFromDate(sdf.parse(requestWindow.getFrom()));
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(requestWindow,
					"Invalid leave from date format. (yyyy-MM-dd)", "Invalid date format",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		try {
			leave.setToDate(sdf.parse(requestWindow.getTo()));
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(requestWindow,
					"Invalid leave from date format. (yyyy-MM-dd)", "Invalid date format",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		leave.setStatus(LeaveStatus.Pending);
		leave.setNextSupervisor(SingletonResource.getLoginStaff().getSupervisor());
		
		SingletonResource.addLeave(leave);

		JOptionPane.showMessageDialog(requestWindow,
				"Leave applyed.", "Apply leave",
				JOptionPane.INFORMATION_MESSAGE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		switch (action) {
		case "apply":
			applyLeave();
			break;
		default:
			break;
		}
	}

}
