package BusinessLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Entities.Staff;
import Resources.SingletonResource;
import Views.MainWindow;
import Views.StaffWindow;

public class StaffBL implements ActionListener {
	private MainWindow mainWindow;
	private StaffWindow staffWindow;
	
	public StaffBL(MainWindow mainWindow, StaffWindow staffWindow){
		this.mainWindow = mainWindow;
		this.staffWindow = staffWindow;
	}

	public void showStaffWindow(){
		this.staffWindow.addActionListener(this);
		
		staffWindow.setClosable(true);
		mainWindow.getInternalPane().add(staffWindow);
		
		staffWindow.setVisible(true);
		
		staffWindow.setStaffs();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		switch (action) {
		case "new":
			staffWindow.setNewMode();
			break;
		case "delete":
			staffWindow.removeStaff(staffWindow.getLoginID());
			JOptionPane.showMessageDialog(staffWindow,
					"Staff deleted.", "Delete staff",
					JOptionPane.INFORMATION_MESSAGE);
			break;
		case "save":
			if(staffWindow.getPassword().equals(staffWindow.getConfirmPassword())){
				saveStaff();
			}else{
				JOptionPane.showMessageDialog(staffWindow,
						"password and confirm password not match.", "password not match",
						JOptionPane.WARNING_MESSAGE);
			}
			break;
		default:
			break;
		}
	}
	
	public void saveStaff(){
		String loginID = staffWindow.getLoginID();
		String firstName = staffWindow.getFirstName();
		String lastName = staffWindow.getLastName();
		String password = staffWindow.getPassword();
		
		Staff staff = SingletonResource.getStaff(loginID);
		
		if(staff==null){
			//New
			staff = new Staff();
			staff.setUserID(loginID);
			staff.setFirstName(firstName);
			staff.setLastName(lastName);
			staff.setPassword(password);
			staff.isDirector(false);
			
			staffWindow.addStaff(staff);
			
			JOptionPane.showMessageDialog(staffWindow,
					"New staff added.", "New staff",
					JOptionPane.INFORMATION_MESSAGE);
		}else{
			//Edit
			staff.setFirstName(firstName);
			staff.setLastName(lastName);
			staff.setPassword(password);
			staff.isDirector(false);			

			JOptionPane.showMessageDialog(staffWindow,
					"Staff updated.", "Edit staff",
					JOptionPane.INFORMATION_MESSAGE);
		}
		staffWindow.setNewMode();
	}
}
