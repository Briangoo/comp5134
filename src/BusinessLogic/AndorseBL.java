package BusinessLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Entities.Leave;
import Entities.LeaveStatus;
import Resources.SingletonResource;
import Views.AndorseWindow;
import Views.MainWindow;

public class AndorseBL implements ActionListener {
	private MainWindow mainWindow;
	private AndorseWindow andorseWindow;
	
	public AndorseBL(MainWindow mainWindow, AndorseWindow andorseWindow){
		this.mainWindow = mainWindow;
		this.andorseWindow = andorseWindow;
	}

	public void showAndorseWindow(){
		this.andorseWindow.addActionListener(this);
		
		andorseWindow.setClosable(true);
		mainWindow.getInternalPane().add(andorseWindow);
		
		andorseWindow.setVisible(true);
		
		andorseWindow.setLeaves();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		switch (action) {
		case "andorse":
			andorse();
			break;
		case "reject":
			reject();
			break;
		default:
			break;
		}
	}
	
	public void andorse(){
		Leave leave = SingletonResource.getLeave(andorseWindow.getLeaveString());
		if(SingletonResource.getLoginStaff().isDirector()){
			leave.setStatus(LeaveStatus.Approved);
		}else{
			leave.setNextSupervisor(SingletonResource.getLoginStaff().getSupervisor());
		}
		JOptionPane.showMessageDialog(andorseWindow,
				"Leave andorsed.", "Andorse leave",
				JOptionPane.INFORMATION_MESSAGE);
		andorseWindow.setLeaves();
		andorseWindow.cleanTxt();
	}

	public void reject(){
		Leave leave = SingletonResource.getLeave(andorseWindow.getLeaveString());
		leave.setStatus(LeaveStatus.Rejected);
		
		JOptionPane.showMessageDialog(andorseWindow,
				"Leave rejected.", "Reject leave",
				JOptionPane.INFORMATION_MESSAGE);
		andorseWindow.setLeaves();
		andorseWindow.cleanTxt();
	}
}
