package BusinessLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Entities.Staff;
import Resources.SingletonResource;
import Views.LoginWindow;
import Views.MainWindow;

public class LoginBL implements ActionListener {
	private LoginWindow loginWindow;
	private MainWindow mainWindow;

	public LoginBL(MainWindow mainWindow, LoginWindow loginWindow) {
		this.loginWindow = loginWindow;
		this.mainWindow = mainWindow;
	}

	public void showLoginWindow() {
		mainWindow.disableMenuBar();

		this.loginWindow.addActionListener(this);
		loginWindow.setClosable(false);
		mainWindow.getInternalPane().add(loginWindow);
		loginWindow.setVisible(true);
	}

	private void login() {
		if (!processLogin(loginWindow.getLoginID(),loginWindow.getPassword())) {
			JOptionPane.showMessageDialog(loginWindow,
					"Login ID or password invalid or invalid staff.", "Invalid login",
					JOptionPane.WARNING_MESSAGE);
		} else {
			mainWindow.enableMenuBar();
			if(SingletonResource.getLoginStaff().isDirector()){
				mainWindow.setDirectorMode();
			}else{
				mainWindow.setStaffMode();
			}
			loginWindow.dispose();
		}
	}

	private Boolean processLogin(String userID, String password) {
		Staff staff = SingletonResource.getStaff(userID);
		if (staff != null && staff.getPassword().equals(password) && staff.isValid()) {
			SingletonResource.setLoginStaff(staff);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		switch (action) {
		case "login":
			login();
			break;
		default:
			break;
		}
	}

}
