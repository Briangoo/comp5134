package Views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Entities.Staff;
import Resources.SingletonResource;

public class AssignWindow extends JInternalFrame implements ActionListener,
		ListSelectionListener {

	private JList<String> _staffList;
	private DefaultListModel<String> _staffListModel;

	private JList<String> _supervisorList;
	private DefaultListModel<String> _supervisorListModel;

	private JButton saveBtn;

	private ActionListener parentListener;

	/**
	 * Create the frame.
	 */
	public AssignWindow(String title) {
		this.setTitle(title);
		this.setSize(new Dimension(600, 400));

		getContentPane().setLayout(new BorderLayout(10, 5));

		JPanel staffListPanel = new JPanel();
		staffListPanel.setLayout(new BorderLayout());

		JLabel staffLbl = new JLabel("Staffs");
		staffListPanel.add(staffLbl, BorderLayout.NORTH);

		_staffList = new JList<String>();
		_staffList.setFixedCellWidth(120);
		_staffListModel = new DefaultListModel<String>();
		_staffList.setModel(_staffListModel);
		_staffList.setName("staff");
		_staffList.addListSelectionListener(this);
		staffListPanel.add(_staffList, BorderLayout.CENTER);

		getContentPane().add(staffListPanel, BorderLayout.WEST);

		JPanel supervisorListPanel = new JPanel();
		supervisorListPanel.setLayout(new BorderLayout());

		JLabel supervisorLbl = new JLabel("Supervisor");
		supervisorListPanel.add(supervisorLbl, BorderLayout.NORTH);

		_supervisorList = new JList<String>();
		_supervisorList.setFixedCellWidth(120);
		_supervisorListModel = new DefaultListModel<String>();
		_supervisorList.setModel(_supervisorListModel);
		_supervisorList.setName("supervisor");
		_supervisorList.addListSelectionListener(this);
		supervisorListPanel.add(_supervisorList, BorderLayout.CENTER);

		JPanel savePanel = new JPanel();
		saveBtn = new JButton("Save");
		saveBtn.setActionCommand("save");
		saveBtn.addActionListener(this);
		savePanel.add(saveBtn);
		supervisorListPanel.add(savePanel, BorderLayout.SOUTH);

		getContentPane().add(supervisorListPanel, BorderLayout.CENTER);

	}

	public void setStaffs() {
		_staffListModel.clear();
		for (Staff staff : SingletonResource.getStaffs()) {
			if (!staff.isDirector()) {
				_staffListModel.addElement(staff.getUserID());
			}
		}
	}

	public Staff getStaff() {
		String userID = _staffList.getSelectedValue();
		if (userID != null && userID.length() > 0) {
			return SingletonResource.getStaff(userID);
		}
		return null;
	}

	public Staff getSupervisor() {
		String userID = _supervisorList.getSelectedValue();
		if (userID != null && userID.length() > 0) {
			return SingletonResource.getStaff(userID);
		}
		return null;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		parentListener.actionPerformed(e);
	}

	public void addActionListener(ActionListener l) {
		parentListener = l;
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (((JList<String>) e.getSource()).getName().equals("staff")) {
			Staff selectedStaff = SingletonResource.getStaff(_staffList
					.getSelectedValue());
			if (selectedStaff != null) {
				_supervisorListModel.clear();
				for (Staff staff : SingletonResource.getStaffs()) {
					if (!selectedStaff.getUserID().equals(staff.getUserID())) {
						if (!staff.isAncestor(selectedStaff)) {
							_supervisorListModel.addElement(staff.getUserID());
						}
					}
				}

				Staff supervisor = selectedStaff.getSupervisor();
				if (supervisor != null) {
					_supervisorList.setSelectedValue(supervisor.getUserID(),
							true);
				}
			}
		}
	}
}
