package Views;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class MainWindow extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JDesktopPane internalPane;
	private JMenuBar menuBar;
	private JMenu menuStaffs;
	private JMenu menuLeaves;
	private JMenuItem menuItemRequestLeave;
	private JMenuItem menuItemResultLeave;
	private ActionListener parentListener;

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1280, 1024);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		menuStaffs = new JMenu("Staffs");
		menuBar.add(menuStaffs);
		
		JMenuItem menuItemAddStaffs = new JMenuItem("Add/Edit Staffs");
		menuItemAddStaffs.setActionCommand("staff");
		menuItemAddStaffs.addActionListener(this);
		menuStaffs.add(menuItemAddStaffs);
		
		JMenuItem menuItemAssignSupervisor = new JMenuItem("Assign Supervisor");
		menuItemAssignSupervisor.setActionCommand("supervisor");
		menuItemAssignSupervisor.addActionListener(this);
		menuStaffs.add(menuItemAssignSupervisor);
		
		menuLeaves = new JMenu("Leaves");
		menuBar.add(menuLeaves);
		
		menuItemRequestLeave = new JMenuItem("Apply leave");
		menuItemRequestLeave.setActionCommand("apply");
		menuItemRequestLeave.addActionListener(this);
		menuLeaves.add(menuItemRequestLeave);
		
		JMenuItem menuItemAndorseLeave = new JMenuItem("Endorse/Reject leaves");
		menuItemAndorseLeave.setActionCommand("endorse");
		menuItemAndorseLeave.addActionListener(this);
		menuLeaves.add(menuItemAndorseLeave);
		
		menuItemResultLeave = new JMenuItem("My leaves");
		menuItemResultLeave.setActionCommand("result");
		menuItemResultLeave.addActionListener(this);
		menuLeaves.add(menuItemResultLeave);
		
		JMenuItem menuLogout = new JMenuItem("Logout");
		menuLogout.setActionCommand("logout");
		menuLogout.addActionListener(this);
		menuBar.add(menuLogout);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		internalPane = new JDesktopPane();
		contentPane.add(internalPane, BorderLayout.CENTER);
	}
	
	public JDesktopPane getInternalPane(){
		return this.internalPane;
	}
	
	public void disableMenuBar(){
		menuBar.setVisible(false);
	}
	
	public void enableMenuBar(){
		menuBar.setVisible(true);
	}
	
	public void setDirectorMode(){
		menuStaffs.setVisible(true);
		menuItemRequestLeave.setVisible(false);
		menuItemResultLeave.setVisible(false);
	}
	
	public void setStaffMode(){
		menuStaffs.setVisible(false);
		menuItemRequestLeave.setVisible(true);
		menuItemResultLeave.setVisible(true);
	}
	
	public void addActionListener(ActionListener l){
		this.parentListener = l;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.parentListener.actionPerformed(e);
	}
	
	
}
