package Views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Entities.Staff;
import Resources.SingletonResource;

public class StaffWindow extends JInternalFrame implements ActionListener,
		ListSelectionListener {

	private JList<String> _staffList;
	private DefaultListModel<String> _staffListModel;

	private JButton newBtn;
	private JTextField loginIDTxt;
	private JTextField firstNameTxt;
	private JTextField lastNameTxt;
	private JPasswordField passwordTxt;
	private JPasswordField confirmPTxt;
	private JButton saveBtn;
	private JButton deleteBtn;

	private ActionListener parentListener;

	/**
	 * Create the frame.
	 */
	public StaffWindow(String title) {
		this.setTitle(title);
		this.setSize(new Dimension(600, 400));

		JPanel staffListPanel = new JPanel();
		staffListPanel.setLayout(new BorderLayout());

		JLabel staffLbl = new JLabel("Staffs");
		staffListPanel.add(staffLbl, BorderLayout.NORTH);

		_staffList = new JList<String>();
		_staffList.setFixedCellWidth(120);
		_staffListModel = new DefaultListModel<String>();
		_staffList.setModel(_staffListModel);
		_staffList.addListSelectionListener(this);
		staffListPanel.add(_staffList, BorderLayout.CENTER);
		
		deleteBtn = new JButton("Delete");
		deleteBtn.setActionCommand("delete");
		deleteBtn.addActionListener(this);
		staffListPanel.add(deleteBtn, BorderLayout.SOUTH);

		getContentPane().add(staffListPanel, BorderLayout.WEST);

		JPanel detailPanel = new JPanel();
		detailPanel.setLayout(new GridLayout(7, 0));

		JPanel newPanel = new JPanel();
		newBtn = new JButton("New Staff");
		newBtn.setActionCommand("new");
		newBtn.addActionListener(this);
		newPanel.add(newBtn);
		detailPanel.add(newPanel);

		JPanel loginIDPanel = new JPanel();
		JLabel loginIDLbl = new JLabel("Login ID: ");
		loginIDPanel.add(loginIDLbl);
		loginIDTxt = new JTextField();
		loginIDTxt.setPreferredSize(new Dimension(100, 24));
		loginIDPanel.add(loginIDTxt);
		detailPanel.add(loginIDPanel);

		JPanel firstNamePanel = new JPanel();
		JLabel firstNameLbl = new JLabel("First name: ");
		firstNamePanel.add(firstNameLbl);
		firstNameTxt = new JTextField();
		firstNameTxt.setPreferredSize(new Dimension(100, 24));
		firstNamePanel.add(firstNameTxt);
		detailPanel.add(firstNamePanel);

		JPanel lastNamePanel = new JPanel();
		JLabel lastNameLbl = new JLabel("Last name: ");
		lastNamePanel.add(lastNameLbl);
		lastNameTxt = new JTextField();
		lastNameTxt.setPreferredSize(new Dimension(100, 24));
		lastNamePanel.add(lastNameTxt);
		detailPanel.add(lastNamePanel);

		JPanel passwordPanel = new JPanel();
		JLabel passwordLbl = new JLabel("Password: ");
		passwordPanel.add(passwordLbl);
		passwordTxt = new JPasswordField();
		passwordTxt.setPreferredSize(new Dimension(100, 24));
		passwordPanel.add(passwordTxt);
		detailPanel.add(passwordPanel);

		JPanel confirmPPanel = new JPanel();
		JLabel confirmPLbl = new JLabel("Confirm Password: ");
		confirmPPanel.add(confirmPLbl);
		confirmPTxt = new JPasswordField();
		confirmPTxt.setPreferredSize(new Dimension(100, 24));
		confirmPPanel.add(confirmPTxt);
		detailPanel.add(confirmPPanel);

		JPanel savePanel = new JPanel();
		saveBtn = new JButton("Save");
		saveBtn.setActionCommand("save");
		saveBtn.addActionListener(this);
		savePanel.add(saveBtn);
		detailPanel.add(savePanel);

		getContentPane().add(detailPanel, BorderLayout.CENTER);
	}

	public void setStaffs() {
		_staffListModel.clear();
		for (Staff staff : SingletonResource.getStaffs()) {
			_staffListModel.addElement(staff.getUserID());
		}
	}

	public void addStaff(Staff staff) {
		SingletonResource.addStaff(staff);
		_staffListModel.addElement(staff.getUserID());
	}
	
	public void removeStaff(String loginID){
		SingletonResource.removeStaff(getLoginID());
		_staffListModel.removeElementAt(_staffList.getSelectedIndex());
	}

	public String getLoginID() {
		return loginIDTxt.getText();
	}

	public String getFirstName() {
		return firstNameTxt.getText();
	}

	public String getLastName() {
		return lastNameTxt.getText();
	}

	public String getPassword() {
		return String.valueOf(passwordTxt.getPassword());
	}

	public String getConfirmPassword() {
		return String.valueOf(confirmPTxt.getPassword());
	}

	public void setNewMode() {
		loginIDTxt.setEnabled(true);
		_staffList.clearSelection();

		loginIDTxt.setText("");
		firstNameTxt.setText("");
		lastNameTxt.setText("");
		passwordTxt.setText("");
		confirmPTxt.setText("");
	}

	public void actionPerformed(ActionEvent e) {
		parentListener.actionPerformed(e);
	}

	public void addActionListener(ActionListener l) {
		parentListener = l;
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		@SuppressWarnings("unchecked")
		String selectedLoginID = (String) ((JList<String>) e.getSource())
				.getSelectedValue();
		if (selectedLoginID != null && !selectedLoginID.equals("")) {
			loginIDTxt.setEnabled(false);

			Staff selectedStaff = SingletonResource.getStaff(selectedLoginID);

			loginIDTxt.setText(selectedLoginID);
			firstNameTxt.setText(selectedStaff.getFirstName());
			lastNameTxt.setText(selectedStaff.getLastName());
			passwordTxt.setText("");
			confirmPTxt.setText("");
		}
	}

}
