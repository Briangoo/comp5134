package Views;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginWindow extends JInternalFrame implements ActionListener {
	
	private JTextField loginIDTxt;
	private JPasswordField passwordTxt;
	private JButton loginBtn;
	private ActionListener parentListener;

	/**
	 * Create the frame.
	 */
	public LoginWindow(String title) {
		this.setTitle(title);
		
		this.setSize(new Dimension(600,400));
		getContentPane().setLayout(new GridLayout(3,0));
		
		JPanel emptyPanel = new JPanel();
		emptyPanel.setBounds(0, 0, 500, 80);
		getContentPane().add(emptyPanel);
		
		JPanel loginPanel = new JPanel();
		loginPanel.setLayout(new GridLayout(2,0));
		JPanel loginIDPanel = new JPanel();
		JLabel loginIDLbl = new JLabel();
		loginIDLbl.setText("Login ID:");
		loginIDPanel.add(loginIDLbl);
		loginIDTxt = new JTextField();
		loginIDTxt.setPreferredSize(new Dimension(100,24));
		loginIDPanel.add(loginIDTxt);
		JPanel passwordPanel = new JPanel();
		JLabel passwordLbl = new JLabel();
		passwordLbl.setText("Password:");
		passwordPanel.add(passwordLbl);
		passwordTxt = new JPasswordField();
		passwordTxt.setPreferredSize(new Dimension(100,24));
		passwordPanel.add(passwordTxt);
		loginPanel.add(loginIDPanel);
		loginPanel.add(passwordPanel);
		getContentPane().add(loginPanel);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setBounds(0, 0, 500, 80);
		loginBtn = new JButton();
		loginBtn.setText("Login");
		loginBtn.setActionCommand("login");
		loginBtn.addActionListener(this);
		buttonPanel.add(loginBtn);
		getContentPane().add(buttonPanel);
	}
	
	public String getLoginID(){
		return loginIDTxt.getText();
	}
	
	public String getPassword(){
		return String.valueOf(passwordTxt.getPassword());
	}
	
	public void actionPerformed(ActionEvent e) {
		parentListener.actionPerformed(e);
	}

	public void addActionListener(ActionListener l){
		parentListener = l;
	}
	
}
