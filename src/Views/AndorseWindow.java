package Views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Entities.Leave;
import Entities.Staff;
import Resources.SingletonResource;

public class AndorseWindow extends JInternalFrame implements ActionListener,
		ListSelectionListener {

	private JList<String> _leaveList;
	private DefaultListModel<String> _leaveListModel;

	private JTextField applyingStaffTxt;
	private JTextField fromTxt;
	private JTextField toTxt;

	private JButton andorseBtn;
	private JButton rejectBtn;

	private ActionListener parentListener;

	/**
	 * Create the frame.
	 */
	public AndorseWindow(String title) {
		this.setTitle(title);
		this.setSize(new Dimension(600, 400));

		getContentPane().setLayout(new BorderLayout(10, 5));

		JPanel leaveListPanel = new JPanel();
		leaveListPanel.setLayout(new BorderLayout());

		JLabel leaveLbl = new JLabel("Leaves waiting for andorse");
		leaveListPanel.add(leaveLbl, BorderLayout.NORTH);

		_leaveList = new JList<String>();
		_leaveList.setFixedCellWidth(150);
		_leaveListModel = new DefaultListModel<String>();
		_leaveList.setModel(_leaveListModel);
		_leaveList.addListSelectionListener(this);
		leaveListPanel.add(_leaveList, BorderLayout.CENTER);

		getContentPane().add(leaveListPanel, BorderLayout.CENTER);

		JPanel detailPanel = new JPanel();
		detailPanel.setLayout(new GridLayout(4, 0));

		JPanel applyingStaffPanel = new JPanel();
		JLabel applyingStaffLbl = new JLabel("Applying staff:");
		applyingStaffPanel.add(applyingStaffLbl);
		applyingStaffTxt = new JTextField();
		applyingStaffTxt.setPreferredSize(new Dimension(120,24));
		applyingStaffTxt.setEnabled(false);
		applyingStaffPanel.add(applyingStaffTxt);
		detailPanel.add(applyingStaffPanel);

		JPanel fromPanel = new JPanel();
		JLabel fromLbl = new JLabel("Leave from:");
		fromPanel.add(fromLbl);
		fromTxt = new JTextField();
		fromTxt.setPreferredSize(new Dimension(120,24));
		fromTxt.setEnabled(false);
		fromPanel.add(fromTxt);
		detailPanel.add(fromPanel);

		JPanel toPanel = new JPanel();
		JLabel toLbl = new JLabel("Leave to:");
		toPanel.add(toLbl);
		toTxt = new JTextField();
		toTxt.setPreferredSize(new Dimension(120,24));
		toTxt.setEnabled(false);
		toPanel.add(toTxt);
		detailPanel.add(toPanel);

		JPanel buttonPanel = new JPanel();
		andorseBtn = new JButton("Andorse");
		andorseBtn.setActionCommand("andorse");
		andorseBtn.addActionListener(this);
		buttonPanel.add(andorseBtn);
		rejectBtn = new JButton("Reject");
		rejectBtn.setActionCommand("reject");
		rejectBtn.addActionListener(this);
		buttonPanel.add(rejectBtn);

		detailPanel.add(buttonPanel);

		getContentPane().add(detailPanel, BorderLayout.EAST);

	}

	public void setLeaves() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		_leaveListModel.clear();
		for (Leave leave : SingletonResource.getLeavesToApprove()) {
			_leaveListModel.addElement(leave.getApplyingStaff().getUserID()
					+ "(" + sdf.format(leave.getFromDate()) + "-"
					+ sdf.format(leave.getToDate()) + ")" + leave.getStatus().toString());
		}
	}
	
	public void cleanTxt(){
		applyingStaffTxt.setText("");
		fromTxt.setText("");
		toTxt.setText("");
	}

	public String getLeaveString() {
		return (String) (_leaveList).getSelectedValue();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		parentListener.actionPerformed(e);
	}

	public void addActionListener(ActionListener l) {
		parentListener = l;
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		@SuppressWarnings("unchecked")
		String selectedLeaveString = (String) ((JList<String>) e.getSource())
				.getSelectedValue();

		if (selectedLeaveString != null && !selectedLeaveString.equals("")) {
			Leave selectedLeave = SingletonResource
					.getLeave(selectedLeaveString);

			applyingStaffTxt.setText(selectedLeave.getApplyingStaff()
					.getUserID());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			fromTxt.setText(sdf.format(selectedLeave.getFromDate()));
			toTxt.setText(sdf.format(selectedLeave.getToDate()));
		}
	}
}
