package Views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class RequestWindow extends JInternalFrame implements ActionListener {

	private JTextField fromTxt;
	private JTextField toTxt;
	private JButton applyBtn;
	private ActionListener parentListener;

	/**
	 * Create the frame.
	 */
	public RequestWindow(String title) {
		this.setTitle(title);
		
		this.setSize(new Dimension(600,400));
		getContentPane().setLayout(new GridLayout(3,0));
		
		JPanel emptyPanel = new JPanel();
		emptyPanel.setBounds(0, 0, 500, 80);
		getContentPane().add(emptyPanel);
		
		JPanel applyPanel = new JPanel();
		applyPanel.setLayout(new GridLayout(2,0));
		JPanel fromPanel = new JPanel();
		JLabel fromLbl = new JLabel();
		fromLbl.setText("Leave from:");
		fromPanel.add(fromLbl);
		fromTxt = new JTextField();
		fromTxt.setPreferredSize(new Dimension(120,24));
		fromPanel.add(fromTxt);
		JPanel toPanel = new JPanel();
		JLabel toLbl = new JLabel();
		toLbl.setText("Leave to:");
		toPanel.add(toLbl);
		toTxt = new JTextField();
		toTxt.setPreferredSize(new Dimension(120,24));
		toPanel.add(toTxt);
		applyPanel.add(fromPanel);
		applyPanel.add(toPanel);
		getContentPane().add(applyPanel);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setBounds(0, 0, 500, 80);
		applyBtn = new JButton();
		applyBtn.setText("Apply");
		applyBtn.setActionCommand("apply");
		applyBtn.addActionListener(this);
		buttonPanel.add(applyBtn);
		getContentPane().add(buttonPanel);
	}
	
	public String getFrom(){
		return fromTxt.getText();
	}
	
	public String getTo(){
		return toTxt.getText();
	}
	
	public void actionPerformed(ActionEvent e) {
		parentListener.actionPerformed(e);
	}

	public void addActionListener(ActionListener l){
		parentListener = l;
	}
	

}
