import java.awt.EventQueue;

import BusinessLogic.MainBL;


public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainBL mainBL = new MainBL();
					mainBL.programStart();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
