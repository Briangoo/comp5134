package Resources;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import Entities.Leave;
import Entities.LeaveStatus;
import Entities.Staff;

public class SingletonResource {
	private static List<Staff> _staffs;
	private static List<Leave> _leaves;
	private static Staff _loginStaff;

	public static Staff getLoginStaff() {
		return _loginStaff;
	}

	public static void setLoginStaff(Staff loginStaff) {
		SingletonResource._loginStaff = loginStaff;
	}

	public static List<Staff> getStaffs() {
		return _staffs;
	}

	public static List<Leave> getLeaves() {
		return _leaves;
	}

	public static void initialize() {
		_staffs = new ArrayList<Staff>();
		_leaves = new ArrayList<Leave>();

		Staff staffA = new Staff();
		Staff staffB = new Staff();
		Staff staffC = new Staff();

		staffA.setUserID("staffA");
		staffA.setFirstName("Staff");
		staffA.setLastName("A");
		staffA.setPassword("a1");
		staffA.isDirector(true);
		staffA.setSupervisor(null);

		staffB.setUserID("staffB");
		staffB.setFirstName("Staff");
		staffB.setLastName("B");
		staffB.setPassword("b2");
		staffB.isDirector(false);
		staffB.setSupervisor(staffA);

		staffC.setUserID("staffC");
		staffC.setFirstName("Staff");
		staffC.setLastName("C");
		staffC.setPassword("c3");
		staffC.isDirector(false);
		staffC.setSupervisor(staffB);

		_staffs.add(staffA);
		_staffs.add(staffB);
		_staffs.add(staffC);
	}

	public static Staff getStaff(String userID) {
		for (Staff staff : _staffs) {
			if (staff.getUserID().equals(userID)) {
				return staff;
			}
		}
		return null;
	}
	
	public static void removeStaff(String userID){
		for(int i = _staffs.size()-1;i>=0;i--){
			if(_staffs.get(i).getUserID().equals(userID)){
				_staffs.remove(i);
				return;
			}
		}
	}

	public static Leave getLeave(String leaveString) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		for (Leave leave : _leaves) {
			if (leaveString.equals(leave.getApplyingStaff().getUserID() + "("
					+ sdf.format(leave.getFromDate()) + "-"
					+ sdf.format(leave.getToDate()) + ")" + leave.getStatus().toString())) {
				return leave;
			}
		}
		return null;
	}

	public static void addStaff(Staff staff) {
		_staffs.add(staff);
	}

	public static void addLeave(Leave leave) {
		_leaves.add(leave);
	}

	public static List<Leave> getLeavesToApprove() {
		List<Leave> approveLeaves = new ArrayList<Leave>();
		for (Leave leave : _leaves) {
			if (leave.getNextSupervisor().getUserID()
					.equals(_loginStaff.getUserID())
					&& leave.getStatus().equals(LeaveStatus.Pending)) {
				approveLeaves.add(leave);
			}
		}
		return approveLeaves;
	}
	
	public static List<Leave> getMyLeaves(){
		List<Leave> myLeaves = new ArrayList<Leave>();
		for(Leave leave:_leaves){
			if(leave.getApplyingStaff().getUserID().equals(_loginStaff.getUserID())){
				myLeaves.add(leave);
			}
		}
		return myLeaves;
	}
}
